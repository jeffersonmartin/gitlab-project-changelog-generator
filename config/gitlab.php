<?php

return [
    'base_url' => env('GITLAB_BASE_URL', 'https://gitlab.com'),
    'api_token' => env('GITLAB_API_TOKEN')
];
