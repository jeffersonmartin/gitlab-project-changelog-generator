<?php

namespace App\Console\Commands;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class MergeRequestsList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merge-requests-list
                            {project_id : The ID of the GitLab project}
                            {--S|start_date= : The start date of the filtered date range}
                            {--E|end_date= : The end date of the filtered date range}
                            {--K|keyword= : Keyword search}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a list of merge requests for a project.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->line('');
        $this->info('Generating a list of merge requests');

        // Check if API token has been set or return an error
        if(!config('gitlab.api_token')) {
            $this->error('You have not set your GitLab API token in the `.env` file.');
            $this->line('1. Visit https://gitlab.com or your private GitLab instance.');
            $this->line('2. Navigate to User Settings > Personal Access Tokens');
            $this->line('3. Create a new token with the `api` scope.');
            $this->line('4. Update the `GITLAB_API_TOKEN` variable in the `.env` file');
            die();
        }

        // Connect to GitLab API using credentials defined in config/hackystack.php
        // and the .env file.
        $gitlab_client = new \Gitlab\Client();
        $gitlab_client->authenticate(config('gitlab.api_token'), \Gitlab\Client::AUTH_HTTP_TOKEN);
        $gitlab_client->setUrl(config('gitlab.base_url'));
        $gitlab_pager = new \Gitlab\ResultPager($gitlab_client);

        // Get details about the GitLab project
        $project = $gitlab_client->projects()->show($this->argument('project_id'));

        $this->line('Project ID: '.$project['id']);
        $this->line('Project Name: '.$project['name_with_namespace']);
        $this->line('Project URL: '.$project['web_url']);
        $this->line('');

        // Parse date range
        $start_date = $this->option('start_date') ? \Carbon\Carbon::parse($this->option('start_date')) : \Carbon\Carbon::parse('2000-01-01');
        $end_date = $this->option('end_date') ? \Carbon\Carbon::parse($this->option('end_date')) : now();

        //
        // Issues
        //

        // Get merge requests for GitLab project
        $merge_requests = $gitlab_pager->fetchAll($gitlab_client->mergeRequests(), 'all', [$this->argument('project_id'), [
            'sort' => 'asc',
            'search' => $this->option('keyword') ? $this->option('keyword') : ''
        ]]);

        // Loop through issues and add to array
        $mr_outputs = [];
        foreach($merge_requests as $merge_request) {

            $mr_updated_at = \Carbon\Carbon::parse($merge_request['updated_at']);

            if($mr_updated_at > $start_date && $mr_updated_at < $end_date) {

                // Add issue to outputs
                $mr_outputs[] = 'Updated '.$mr_updated_at->format('Y-m-d').' - !'.$merge_request['iid'].' - '.$merge_request['title'];
            }

        }

        // Convert the compiled array to a collection and sort alphabetically
        // Note: The sort() functionality sorts capitalized and lowercase letters
        // separately.
        $mr_collection = collect($mr_outputs);
        $mr_collection_outputs = $mr_collection->sort()->values()->all();

        // Show list of issues in console output
        $this->line('');
        $this->comment('### Issues ('.$mr_collection->count().')');
        foreach($mr_collection_outputs as $mr_collection_row) {
            $this->line('* '.$mr_collection_row);
        }

    }

}
