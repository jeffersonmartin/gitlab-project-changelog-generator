<?php

namespace App\Console\Commands;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class GenerateChangelog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate-changelog
                            {project_id : The ID of the GitLab project}
                            {--S|start_date= : The start date of the filtered date range}
                            {--E|end_date= : The end date of the filtered date range}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a list of changelog entries based on merge request descriptions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->line('');
        $this->info('Generating a Changelog');

        // Check if API token has been set or return an error
        if(!config('gitlab.api_token')) {
            $this->error('You have not set your GitLab API token in the `.env` file.');
            $this->line('1. Visit https://gitlab.com or your private GitLab instance.');
            $this->line('2. Navigate to User Settings > Personal Access Tokens');
            $this->line('3. Create a new token with the `api` scope.');
            $this->line('4. Update the `GITLAB_API_TOKEN` variable in the `.env` file');
            die();
        }

        // Connect to GitLab API using credentials defined in config/hackystack.php
        // and the .env file.
        $gitlab_client = new \Gitlab\Client();
        $gitlab_client->authenticate(config('gitlab.api_token'), \Gitlab\Client::AUTH_HTTP_TOKEN);
        $gitlab_client->setUrl(config('gitlab.base_url'));
        $gitlab_pager = new \Gitlab\ResultPager($gitlab_client);

        // Get details about the GitLab project
        $project = $gitlab_client->projects()->show($this->argument('project_id'));

        $this->line('Project ID: '.$project['id']);
        $this->line('Project Name: '.$project['name_with_namespace']);
        $this->line('Project URL: '.$project['web_url']);
        $this->line('');

        //
        // Merge Requests
        //

        // Get merge requests for GitLab project
        $merge_requests = $gitlab_pager->fetchAll($gitlab_client->mergeRequests(), 'all', [$this->argument('project_id'), [
            'sort' => 'asc',
            'state' => 'merged',
            'created_after' => $this->option('start_date') ? \Carbon\Carbon::parse($this->option('start_date')) : \Carbon\Carbon::parse($project['created_at']),
            'created_before' => $this->option('end_date') ? \Carbon\Carbon::parse($this->option('end_date')) : now(),
        ]]);

        // Loop through merge requests and add to array
        $mr_outputs = [];
        foreach($merge_requests as $merge_request) {

            // Add merge request to outputs
            $mr_outputs[] = $merge_request['title'].' - '.$merge_request['references']['short'].' - @'.$merge_request['author']['username'];

        }

        // Convert the compiled array to a collection and sort alphabetically
        // Note: The sort() functionality sorts capitalized and lowercase letters
        // separately.
        $mr_collection = collect($mr_outputs);
        $mr_collection_outputs = $mr_collection->sort()->values()->all();

        // Show list of merge requests in console output
        $this->line('');
        $this->comment('### Merge Requests ('.$mr_collection->count().')');
        foreach($mr_collection_outputs as $mr_collection_row) {
            $this->line('* '.$mr_collection_row);
        }

        //
        // Commits
        //

        // Get commits for project between start and end date
        $commits = $gitlab_pager->fetchAll($gitlab_client->repositories(), 'commits', [
            $this->argument('project_id'),
            [
                'since' => $this->option('start_date') ? \Carbon\Carbon::parse($this->option('start_date')) : \Carbon\Carbon::parse($project['created_at']),
                'until' => $this->option('end_date') ? \Carbon\Carbon::parse($this->option('end_date')) : now(),
            ]
        ]);

        // Loop through commits and add values to array
        foreach($commits as $commit) {
            $commit_outputs[] = $commit['title'].' - '.$commit['short_id'];
        }

        // Convert the compiled array to a collection and sort alphabetically
        // Note: The sort() functionality sorts capitalized and lowercase letters
        // separately.
        $commit_collection = collect($commit_outputs);
        $commit_collection_outputs = $commit_collection->sort()->values()->all();

        // Show list of merge requests in console output
        $this->line('');
        $this->comment('### Commits ('.$commit_collection->count().')');
        foreach($commit_collection_outputs as $commit_collection_row) {
            $this->line('* '.$commit_collection_row);
        }

        $this->line('');
        $this->line('<bg=cyan>--------------------------------------------------------------------------------</>');
        $this->line('Copy the outputs above this line to the Changelog file.');

    }

}
