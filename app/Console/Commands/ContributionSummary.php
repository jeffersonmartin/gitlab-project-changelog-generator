<?php

namespace App\Console\Commands;

use App\Models;
use App\Services;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class ContributionSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contribution-summary
                            {project_id : The ID of the GitLab project}
                            {--S|start_date= : The start date of the filtered date range}
                            {--E|end_date= : The end date of the filtered date range}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a summary of merge requests and commits for a date range.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->line('');
        $this->info('Contribution Summary');

        // Check if API token has been set or return an error
        if(!config('gitlab.api_token')) {
            $this->error('You have not set your GitLab API token in the `.env` file.');
            $this->line('1. Visit https://gitlab.com or your private GitLab instance.');
            $this->line('2. Navigate to User Settings > Personal Access Tokens');
            $this->line('3. Create a new token with the `api` scope.');
            $this->line('4. Update the `GITLAB_API_TOKEN` variable in the `.env` file');
            die();
        }

        // Connect to GitLab API using credentials defined in config/hackystack.php
        // and the .env file.
        $gitlab_client = new \Gitlab\Client();
        $gitlab_client->authenticate(config('gitlab.api_token'), \Gitlab\Client::AUTH_HTTP_TOKEN);
        $gitlab_client->setUrl(config('gitlab.base_url'));
        $gitlab_pager = new \Gitlab\ResultPager($gitlab_client);

        // Get details about the GitLab project
        $project = $gitlab_client->projects()->show($this->argument('project_id'));

        $this->line('Project ID: '.$project['id']);
        $this->line('Project Name: '.$project['name_with_namespace']);
        $this->line('Project URL: '.$project['web_url']);
        $this->line('');

        // Define quarters statically since Carbon isn't adding months correctly
        $quarters = [
            [
                'name' => 'FY20-Q4',
                'start_date' => '2019-10-01 00:00:00',
                'end_date' => '2020-02-01 00:00:00'
            ],
            [
                'name' => 'FY21-Q1',
                'start_date' => '2020-02-01 00:00:00',
                'end_date' => '2020-05-01 00:00:00'
            ],
            [
                'name' => 'FY21-Q2',
                'start_date' => '2020-05-01 00:00:00',
                'end_date' => '2020-08-01 00:00:00'
            ],
            [
                'name' => 'FY21-Q3',
                'start_date' => '2020-08-01 00:00:00',
                'end_date' => '2020-11-01 00:00:00'
            ],
            [
                'name' => 'FY21-Q4',
                'start_date' => '2020-11-01 00:00:00',
                'end_date' => '2021-02-01 00:00:00'
            ],

        ];

        //
        // Contributions
        //

        foreach($quarters as $quarter) {

            $start_date = Carbon::parse($quarter['start_date']);
            $end_date = Carbon::parse($quarter['end_date']);

            // Get merge requests between start and end date
            $merge_requests = $gitlab_pager->fetchAll($gitlab_client->mergeRequests(), 'all', [$this->argument('project_id'), [
                'sort' => 'asc',
                'state' => 'merged',
                'created_after' => $start_date,
                'created_before' => $end_date,
            ]]);

            // Get commits for project between start and end date
            $commits = $gitlab_pager->fetchAll($gitlab_client->repositories(), 'commits', [
                $this->argument('project_id'),
                [
                    'since' => $start_date,
                    'until' => $end_date,
                ]
            ]);

            $contributions[] = [
                'quarter' => $quarter['name'],
                'mr_count' => count($merge_requests),
                'commit_count' => count($commits)
            ];

        }

        // Total Contributions

        // Get merge requests between start and end date
        $merge_requests = $gitlab_pager->fetchAll($gitlab_client->mergeRequests(), 'all', [$this->argument('project_id'), [
            'sort' => 'asc',
            'state' => 'merged',
        ]]);

        // Get commits for project between start and end date
        $commits = $gitlab_pager->fetchAll($gitlab_client->repositories(), 'commits', [
            $this->argument('project_id'),
            []
        ]);

        $contributions[] = [
            'quarter' => 'Total',
            'mr_count' => count($merge_requests),
            'commit_count' => count($commits)
        ];

        //dd($contributions);
        //
        $table_rows = [];

        $table_headers[] = 'Project';
        $table_headers[] = 'Metric';
        $table_rows[0][] = $project['path'];
        $table_rows[0][] = 'Merge Requests';
        $table_rows[1][] = $project['path'];
        $table_rows[1][] = 'Commits';

        foreach($contributions as $time_period) {
            $table_headers[] = $time_period['quarter'];
            $table_rows[0][] = $time_period['mr_count'];
            $table_rows[1][] = $time_period['commit_count'];
        }

        //dd($table_rows);

        $this->table(
            $table_headers,
            $table_rows
        );
/*
        $this->table(
            $table_headers,
            $commit_table_rows
        );
*/
        /*
        $this->table(
            ['Quarter', 'MR Count', 'Commit Count'],
            $contributions
        );
        */

        $this->line('');

    }

}
